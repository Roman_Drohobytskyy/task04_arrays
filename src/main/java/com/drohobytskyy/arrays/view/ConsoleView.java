package com.drohobytskyy.arrays.view;

import com.drohobytskyy.arrays.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class ConsoleView {
    public static Logger logger = LogManager.getLogger(ConsoleView.class);
    private static volatile ConsoleView _instance = null;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private static Controller controller = new Controller();

    private ConsoleView() {
    }

    public static synchronized ConsoleView getInstance() {
        if (_instance == null)
            synchronized (ConsoleView.class) {
                if (_instance == null)
                    _instance = new ConsoleView();
            }
        return _instance;
    }

    public static void execute() {
        try {
            runMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void runMenu() throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputText = "";

        for (;;) {
            System.out.println("---------------------------------------------------------");
            System.out.println("Please choose an appropriate item from menu:");
            System.out.println("1. Join two arrays                              ->      1");
            System.out.println("2. Join two arrays (only unique numbers)        ->      2");
            System.out.println("3. Delete numbers which are duplicated 3+ times ->      3");
            System.out.println("4. Delete all duplications in series            ->      4");
            System.out.println("5. Run Array Game                               ->      5");
            System.out.println("6. Show handmade String container =)            ->      6");
            System.out.println("7. Show TwoStringsContainer sorting             ->      7");
            System.out.println("8. If you want to exit from the application     ->   exit");
            System.out.print("...........  ");

            inputText = bufferedReader.readLine().toLowerCase();
            if (inputText.equals("1")) {
                controller.joinOuterTwoArrays();
            } else if (inputText.equals("2")) {
                controller.joinFullOuterOfTwoArrays();
            } else if (inputText.equals("3")) {
                controller.deleteNumbersRepeatedThreeOrMoreTimes();
            } else if (inputText.equals("4")) {
                controller.deleteDuplicatesInSeriesOfNumbers();
            }else if (inputText.equals("5")) {
                controller.runArrayGame();
            }else if (inputText.equals("6")) {
                controller.containerExecute();
            }else if (inputText.equals("7")) {
                controller.twoStringsContainerExecute();
            } else if (inputText.equals("exit")) {
                break;
            } else {
                System.out.println("Incorrect input! Please try again.");
            }
        }
        System.out.println("See you next time! Have a nice day!");
    }


}