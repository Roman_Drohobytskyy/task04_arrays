package com.drohobytskyy.arrays.controller;

import com.drohobytskyy.arrays.model.ArrayTasks;
import com.drohobytskyy.arrays.model.ArrayGame;
import com.drohobytskyy.arrays.model.Container;
import com.drohobytskyy.arrays.model.TwoStringsContainer;

public class Controller {

    public void joinOuterTwoArrays() {
        ArrayTasks.joinOuterTwoArrays();
    }

    public void joinFullOuterOfTwoArrays() {
        ArrayTasks.joinFullOuterOfTwoArrays();
    }

    public void deleteNumbersRepeatedThreeOrMoreTimes() {
        ArrayTasks.deleteNumbersRepeatedThreeOrMoreTimes();
    }

    public void deleteDuplicatesInSeriesOfNumbers() {
        ArrayTasks.deleteDuplicatesInSeriesOfNumbers();
    }

    public void runArrayGame() {
        ArrayGame.runArrayGame();
    }

    public void containerExecute() {
        Container.execute();
    }

    public void twoStringsContainerExecute() {
        TwoStringsContainer.execute();
    }
}
