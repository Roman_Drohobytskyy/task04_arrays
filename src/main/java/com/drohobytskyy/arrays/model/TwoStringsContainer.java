package com.drohobytskyy.arrays.model;

import com.drohobytskyy.arrays.view.ConsoleView;
import java.util.*;

public class TwoStringsContainer implements Comparable<TwoStringsContainer> {
    private static ConsoleView view = ConsoleView.getInstance();
    private String string1 = new String();
    private String string2 = new String();

    public TwoStringsContainer(String string1, String string2) {
        this.string1 = string1;
        this.string2 = string2;
    }

    public static void execute() {
        TwoStringsContainer[] twoStringsContainersArray = new TwoStringsContainer[]{
                        new TwoStringsContainer("Ukraine", "Kyiv"),
                        new TwoStringsContainer("Poland", "Warsaw"),
                        new TwoStringsContainer("Czech Republic", "Prague"),
                        new TwoStringsContainer("Belarus", "Minsk"),};
        List<TwoStringsContainer> containersList = new ArrayList();
        fillArrayListFromArray(containersList, twoStringsContainersArray);
        view.logger.info("Array                :" + Arrays.toString(twoStringsContainersArray));
        Arrays.sort(twoStringsContainersArray);
        view.logger.info("Sorted by the country:" + Arrays.toString(twoStringsContainersArray));
        view.logger.info("ArrayList            :" + containersList);
        Comparator<TwoStringsContainer> comparator = (o1, o2) -> o1.string2.compareTo(o2.string2);
        containersList.sort(comparator);
        view.logger.info("Sorted by the capital:" + containersList);
        view.logger.info("Binary search        : Collections.binarySearch(containersList,"
                + " new TwoStringsContainer (null, \"Minsk\"), comparator): "
                + Collections.binarySearch(containersList, new TwoStringsContainer(null, "Minsk"), comparator));
    }

    private static void fillArrayListFromArray(List<TwoStringsContainer> list, TwoStringsContainer[] array) {
        for (TwoStringsContainer container: array) {
            list.add(container);
        }
    }

    @Override
    public int compareTo(TwoStringsContainer o) {
        return this.string1.compareTo(o.string1);
    }

    @Override
    public String toString() {
        return "{" +
                "country='" + string1 + '\'' +
                ", capital='" + string2 + '\'' +
                '}';
    }
}
