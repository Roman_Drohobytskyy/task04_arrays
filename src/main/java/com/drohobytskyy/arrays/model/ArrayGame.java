package com.drohobytskyy.arrays.model;

import java.util.Arrays;
import java.util.Random;
import com.drohobytskyy.arrays.view.ConsoleView;

public class ArrayGame {
    private final static int HERO_START_POINTS = 25;
    private final static int ARTIFACT_MINIMUM_POINTS = 10;
    private final static int ARTIFACT_MAXIMUM_POINTS = 80;
    private final static int MONSTER_MINIMUM_POINTS = 5;
    private final static int MONSTER_MAXIMUM_POINTS = 100;
    private final static int SIZE_OF_ARRAY = 11; // Hero's start points + 10 doors;
    private static ConsoleView view = ConsoleView.getInstance();
    private static Random random = new Random();
    private static int[] arrayOfDoors = new int[SIZE_OF_ARRAY];

    public static void runArrayGame() {
        fillArrayRandom(arrayOfDoors);
        printFinishGameArray(arrayOfDoors);
        printDoorsToWin(arrayOfDoors);
    }

    public static void fillArrayRandom(int[] array) {
        array[0] = HERO_START_POINTS;
        for (int i = 1; i < array.length; i++) {
            if (random.nextBoolean()) {
                array[i] = random.nextInt(ARTIFACT_MAXIMUM_POINTS
                        - ARTIFACT_MINIMUM_POINTS + 1) + ARTIFACT_MINIMUM_POINTS;
            } else {
                array[i] = random.nextInt(MONSTER_MAXIMUM_POINTS
                        - MONSTER_MINIMUM_POINTS + 1) + MONSTER_MINIMUM_POINTS;
                array[i] *= -1;
            }
        }
        view.logger.info(Arrays.toString(array));
    }

    public static void printFinishGameArray(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(array[0]).append(", ");
        boolean isHeroDead = false;
        int[] result = array.clone();
        int i;
        for (i = 1; i < result.length; i++) {
            result[i] += result[i-1];
            stringBuilder.append(result[i]).append(", ");
            if (result[i] < 0) {
                isHeroDead = true;
                break;
            }
        }
        view.logger.info(Arrays.toString(result));
        if (isHeroDead) {
            view.logger.info("Hero is dead on a step " + i);
        } else {
            view.logger.info("Hero is winner!!! ");
        }
    }

    public static void printDoorsToWin(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        if (sumOfArray(array) < 0) {
            view.logger.info("Hero can't win =(");
        } else {
            view.logger.info("Hero can win =)");
            for (int i = 1; i < array.length; i++) {
                if (array[i] > 0) {
                    stringBuilder.append(i + " (" + array[i] + "), ");
                }
            }
            for (int i = 1; i < array.length; i++) {
                if (array[i] < 0) {
                    stringBuilder.append(i + " (" + array[i] + "), ");
                }
            }
            view.logger.info(stringBuilder.toString());
        }
    }

    public static int sumOfArray(int[] array) {
        int sum = 0;
        for (int element: array) {
            sum += element;
        }
        return sum;
    }

}
