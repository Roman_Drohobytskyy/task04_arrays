package com.drohobytskyy.arrays.model;

import com.drohobytskyy.arrays.view.ConsoleView;

import java.util.Arrays;

public class Container {
    private static final int START_ARRAY_SIZE = 10;
    private static final double MULTIPLIER_FOR_EXPANSION = 1.2;
    private static ConsoleView view = ConsoleView.getInstance();
    private int indexOfLastElement = 0;
    private String[] innerArray = new String[10];

    public void add(String s) {
        if (indexOfLastElement >= (innerArray.length)) {
            resizeInnerArray();
        }
        innerArray[indexOfLastElement++] = s;
    }

    public void add(String... strings) {
        for (int i = 0; i < strings.length; i++) {
            if (indexOfLastElement >= (innerArray.length)) {
                resizeInnerArray();
            }
            innerArray[indexOfLastElement++] = strings[i];
        }
    }

    private void resizeInnerArray() {
        String[] tempArray = innerArray.clone();
        innerArray = new String[(int)(tempArray.length * MULTIPLIER_FOR_EXPANSION)];
        System.arraycopy(tempArray, 0, innerArray, 0, tempArray.length);
        tempArray = null;
    }

    public String get(int i) {
        return innerArray[i];
    }

    public static void execute() {
        Container container = new Container();
        container.add("1");
        container.add("2");
        container.add("3");
        container.add("4");
        container.add("5");
        container.add("6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17");

        view.logger.info(container);
    }

    @Override
    public String toString() {
        String[] realArray = new String[indexOfLastElement];
        System.arraycopy(innerArray, 0, realArray, 0, realArray.length);
        return "Container{" +
                "innerArray=" + Arrays.toString(realArray) +
                '}' + "; Length = " + innerArray.length + ", count of elements = " + indexOfLastElement;
    }
}
