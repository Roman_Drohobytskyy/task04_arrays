package com.drohobytskyy.arrays.model;

import java.util.Arrays;
import java.util.Random;
import com.drohobytskyy.arrays.view.ConsoleView;

public class ArrayTasks {
    private final static int ARRAY_LENGTH = 15;
    private final static int MAXIMUM_VALUE_IN_ARRAY = 6;
    private static Random random = new Random();
    private static ConsoleView view = ConsoleView.getInstance();
    private static int[] array1 = new int[ARRAY_LENGTH];
    private static int[] array2 = new int[ARRAY_LENGTH];

    public static void fillArraysRandomly() {
        fillArrayRandom(array1);
        fillArrayRandom(array2);
    }

    public static void main(String[] args) {
        fillArraysRandomly();
        joinOuterTwoArrays();
    }

    public static void joinOuterTwoArrays() {
        fillArraysRandomly();
        joinOuterTwoArrays(array1, array2);
    }

    public static void joinFullOuterOfTwoArrays() {
        fillArraysRandomly();
        joinFullOuterOfTwoArrays(array1, array2);
    }

    public static void deleteNumbersRepeatedThreeOrMoreTimes() {
        deleteNumbersRepeatedThreeOrMoreTimes(array1);
    }

    public static void deleteDuplicatesInSeriesOfNumbers() {
        fillArraysRandomly();
        deleteDuplicatesInSeriesOfNumbers(array1);    }

    private static void fillArrayRandom(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(MAXIMUM_VALUE_IN_ARRAY);
        }
    }

    private static int[] joinOuterTwoArrays(int[] array1, int[] array2) {
        view.logger.info("Array 1   : " + Arrays.toString(array1));
        view.logger.info("Array 2   : " + Arrays.toString(array2));
        int[] result = new int[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        view.logger.info("Outer join: " + Arrays.toString(result));
        return result;
    }

    private static int[] joinFullOuterOfTwoArrays(int[] array1, int[] array2) {
        view.logger.info("Array 1        : " + Arrays.toString(array1));
        view.logger.info("Array 2        : " + Arrays.toString(array2));
        int[] tempArray = new int[array1.length + array2.length];
        int indexOfTempArray = 0;
        for (int i = 0; i < array1.length; i++) {
            if (!consistValue(array2, array1[i]) && !consistValue(tempArray, array1[i])) {
                tempArray[indexOfTempArray++] = array1[i];
            }
        }
        for (int i = 0; i < array2.length; i++) {
            if (!consistValue(array1, array2[i]) && !consistValue(tempArray, array2[i])) {
                tempArray[indexOfTempArray++] = array2[i];
            }
        }
        int[] result = new int[indexOfTempArray];
        System.arraycopy(tempArray, 0, result, 0, result.length);
        view.logger.info("Full outer join: " + Arrays.toString(result));
        return result;
    }

    private static boolean consistValue(int[] array, int value) {
        boolean isValueInArray = false;
        for (int element : array) {
            if (element == value) {
                isValueInArray = true;
                break;
            }
        }
        return isValueInArray;
    }

    private static int[] deleteNumbersRepeatedThreeOrMoreTimes(int[] array) {
        view.logger.info("Array 1   : " + Arrays.toString(array1));
        int[] tempArray = array.clone();
        int[] checkedNumbers = new int[array.length];
        Arrays.fill(checkedNumbers, Integer.MIN_VALUE);
        view.logger.trace(Arrays.toString(tempArray));
        for (int i = 0; i < array.length; i++) {
            int valueToCompare = array[i];
            int iteratorOfCheckedNumbers = 0;
            if (!consistValue(checkedNumbers, valueToCompare)) {
                int count = countOfOccurrencesOfNumber(tempArray, valueToCompare);
                if (count > 2) {
                    view.logger.trace("number " + valueToCompare + ": " + count + " times");
                    for (int k = 0; k < tempArray.length; ) {
                        if (tempArray[k] == valueToCompare) {
                            tempArray = deleteNumberFromArray(tempArray, k);
                            view.logger.trace(Arrays.toString(tempArray));
                        } else {
                            k++;
                        }
                    }
                }
                checkedNumbers[iteratorOfCheckedNumbers++] = array[i];
            }
        }
        view.logger.info("Result   : " + Arrays.toString(tempArray));
        return tempArray;
    }

    private static int[] deleteNumberFromArray(int[] array, int index) {
        int[] result = new int[array.length - 1];
        System.arraycopy(array, 0, result, 0, index);
        System.arraycopy(array, index + 1, result, index, array.length - index - 1);
        return result;
    }

    private static int countOfOccurrencesOfNumber(int[] array, int number) {
        int result = 0;
        for (int element : array) {
            if (element == number) {
                result++;
            }
        }
        return result;
    }

    private static int[] deleteDuplicatesInSeriesOfNumbers(int[] array) {
        view.logger.info("Array 1 : " + Arrays.toString(array));

        int[] result = array.clone();
        for (int i = 0; i < result.length - 1; ) {
            while ((i < result.length - 1) && (result[i + 1] == result [i])) {
                result = deleteNumberFromArray(result, i+1);
            }
            i++;
        }
        view.logger.info("Result  : " + Arrays.toString(result));
        return result;
    }

}
